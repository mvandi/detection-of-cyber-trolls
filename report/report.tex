\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{verbatim}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}

\usepackage{amsmath}

\usepackage{float}
\usepackage{arydshln}
\usepackage{graphicx}

% paragraph settings
\setlength{\parskip}{0.25em}

% table settings
\setlength\dashlinedash{0.5pt}
\setlength\dashlinegap{1.5pt}
\setlength\arrayrulewidth{0.3pt}

\title{Using Deep Learning for Cyber-Trolls detection}
\author{Mattia Vandi}
\date{}

\begin{document}

\maketitle

\section{Business understanding}

\subsection{Objectives}

The primary objective of this labor, from a business perspective, is to detect wheter a tweet is written by a Cyber-Troll or not given the textual content of the tweet.

A Cyber-Troll is a person who starts quarrels or upsets people on the Internet to distract and sow discord by posting inflammatory and digressive, extraneous, or off-topic messages in an online community (such as a newsgroup, forum, chat room, or blog) with the intent of provoking readers into displaying emotional responses and normalizing tangential discussion, whether for the troll's amusement or a specific gain.

Twitter is an online news and social networking service on which users post and interact with messages known as ``tweets''.
%
A tweet is a short message (originally restricted to $140$ characters, but later it was doubled to $280$ characters for all languages except Chinese, Japanese, and Korean) which contains plain text.

\subsection{Project plan}

\begin{comment}

Here you'll describe the plan for achieving the data mining and business goals.
%
The plan should specify the steps to be performed during the rest of the project, including the initial selection of tools and techniques.

\end{comment}

To achieve the data mining and business goals Keras\footnote{\url{http://keras.io}} was used as tool to build a classification model to predict if a tweet is either Cyber-Aggressive or not.

\subsection{Business success criteria}

\begin{comment}

Here you'll lay out the criteria that you'll use to determine whether the project has been successful from the business point of view.
%
These should ideally be specific and measurable, for example reduction of customer churn to a certain level, however sometimes it might be necessary to have more subjective criteria such as “give useful insights into the relationships.” If this is the case then it needs to be clear who it is that makes the subjective judgment.

\end{comment}

This labor can be considered successful if, following the verification for the presence of aggressive content in a tweet (using the model built during this work) there will be a substantial decrease in the number of aggressive tweets on the platform.

\section{Data understanding}

\subsection{Data description}

The data is represented using the JSON\footnote{\url{https://www.json.org/}} format, the entire dataset was stored in a single file to make uploading easier and faster.

$20,001$ samples were collected, for each sample the following information is available:

\begin{itemize}
%
    \item \textbf{content}: textual content of a ``tweet''.
%
    \item \textbf{annotation}:
%
    \begin{itemize}
%
        \item \textbf{notes}: additional textual notes on the sample in question.
%
        \item \textbf{label}: expected output that the model should predict once it will be training.
%
    \end{itemize}
%
    \item \textbf{extras}: additional information about the content of the sample.
%
    \item \textbf{metadata}:
%
    \begin{itemize}
%
        \item \textbf{first\_done\_at}: timestamp of the first manual labeling.
%
        \item \textbf{last\_updated\_at}: timestamp of the last alteration.
%
        \item \textbf{last\_updated\_by}: identifier of the user who last altered the sample.
%
        \item \textbf{status}: status of the sample:
%
        \begin{itemize}
%
            \item \texttt{done}: the sample has been manually labeled.
%
            \item \texttt{skipped}: the sample has not been labeled yet.
%
            \item \texttt{deleted}: the sample has been stripped out from the dataset.
%
        \end{itemize}
%
    \end{itemize}
%

\end{itemize}

\subsection{Data exploration}

Following a first analysis of the dataset, it has emerged the presence of $259,422$ distinct words (of which $31,726$ are unique) and an average word frequency of $8.2$.

Word frequency refers to the average number of occurrences of all the words in all documents.
%
That is, for each word the number of occurrences of the given word in each document is counted, then it is divided by the number of documents.
%
Finally, the word frequency for each word is averaged.

More formally:

\begin{equation}
    \text{F}\left(w, D\right) = \frac{\sum_{i=1}^{\left|D\right|}{\text{C}\left(w, D_i\right)}}{\left|D\right|}
\end{equation}

where:

\begin{itemize}
%
    \item $\left|D\right|$ is denotes the number of documents in $D$.
%
    \item $\text{C}\left( \cdot \right)$ is a function that counts the number of occurrences of a word $w$ in a document $d$:

    \begin{equation}
        \text{C}\left(w, d\right) = \sum_{i=1}^{\left|d\right|}{\delta_{w,d_i}}
    \end{equation}
%
    \begin{itemize}
%
        \item $\left|d\right|$ denotes the number of words in a document, i.e. the length of the document.
%
        \item $\delta_{w,D_i}$ is the Kronecker Delta \cite{Weisstein_2002} between the given word and the $i$--th word in the document, i.e. $1$ if the $i$--th word in the document $d_i$ is equal to $w$, otherwise $0$.
%
    \end{itemize}
%
\end{itemize}

\section{Data preparation}

\subsection{Data selection}

After a careful analysis of the dataset, the attributes relevant to the analysis were selected.
%
The decisions taken regarding each attribute in the dataset are explained in the table below.

\begin{table}[H]
\centering

\begin{tabular}{c|c|c}
\textbf{Attribute} & \textbf{Decision} & \textbf{Motivation} \\
\hline\hline
\texttt{content}   & included          & \multicolumn{1}{p{7.5cm}}{This attribute contains the textual representation of the ``tweet'', which is we want to classify (i.e. an input for our model).} \\ \hdashline
\texttt{notes}     & excluded          & \multicolumn{1}{p{7.5cm}}{This attribute has always been left blank, therefore this attribute is not relevant for the analysis and cannot be used as input for the model.} \\ \hdashline
\texttt{label}     & included          & \multicolumn{1}{p{7.5cm}}{This attribute contains the target label for this sample, which is we want to predict (i.e. the output of our model).} \\ \hdashline
\texttt{extras}    & excluded          & \multicolumn{1}{p{7.2cm}}{This attribute has always been assigned to the value \texttt{null}, therefore this attribute is not relevant for the analysis and cannot be used as input for the model.} \\ \hdashline
\texttt{metadata}  & excluded          & \multicolumn{1}{p{7.5cm}}{This attribute contains automatically generated meta-information, therefore this attribute is not relevant for the analysis and cannot be used as input for the model.} \\
\end{tabular}
\end{table}

\subsection{Data preparation}

To prepare the data to be provided as input to the learning model the data set content was first converted to lower case then tokenized.

Following the split of the data set into the training set, validation set and test set (described in \ref{td}) a vocabulary of the words contained in the training set was built.
%
The words that occurred less than $5$ times have been removed from the vocabulary.

Finally, the word tokens in the training set, validation set and test set were replaced with the corresponding token indexes in the dictionary.
%
The tokens not present in the vocabulary were converted to the $0$-th index in the vocabulary.
%
The sequences were padded to make them of a fixed length to obtain a matrix with one row for each tweet.

One-hot encoding\footnote{\url{https://machinelearningmastery.com/why-one-hot-encode-data-in-machine-learning}} has been used to transform labels into a $\left[0, 1\right]$ vector when the label marks the content as cyber-aggressive, $\left[1, 0\right]$ otherwise.

\section{Modeling}

\subsection{Model selection}

A recurrent neural network \cite{Mikolov_2010} with backpropagation through time \cite{Werbos_1990} was adopted for the construction of the model.

\subsection{Test design} \label{td}

Error rate and accuracy have been chosen as metrics to evaluate the quality of the produced model.

The dataset was split into three parts: $75\%$ of the samples were used to train the network, $12,5\%$ for validation purposes at the end of each training epoch and the remaining $12,5\%$ for performance verification at the end of the training.

\begin{table}[H]
\centering

\begin{tabular}{c|c}
    \textbf{Data set}                   & \textbf{Samples \#} \\
    \hline\hline
    \multicolumn{1}{l|}{Training set}   & \multicolumn{1}{r}{$15,000$} \\ \hdashline
    \multicolumn{1}{l|}{Validation set} & \multicolumn{1}{r}{$5,000$}  \\ \hdashline
    \multicolumn{1}{l|}{Test set}       & \multicolumn{1}{r}{$5,001$}  \\
\end{tabular}

\caption{Summary of the dataset division in training set, validation set and test set.}
\end{table}

\subsection{Model construction}

The model is composed of an input layer, two hidden layers and an output layer:

\begin{itemize}
%
    \item \textbf{Input}: the input layer has the purpose of translating the indexes of the words that make up the tweet into real-valued vectors.
%
    The weights used in this layer have been pre-trained using the GloVe \cite{Pennington_2014} unsupervised learning algorithm.
%
    The Word2Vec \cite{Mikolov_2013} model was trained for $100$ iterations over a vocabulary built upon the training set using a vector size of $300$.
%
    \item \textbf{Hidden 1}: the first hidden layer is a gated recurrent unit \cite{Chung_2014} layer with $256$ hidden units, using the \textit{inverse tangent} as activation function and the \textit{sigmoid} as recurrent activation function.
%
    A $40\%$ dropout \cite{Srivastava_2014} rate was used to avoid the network overfitting.
%
    \item \textbf{Hidden 2}: the second hidden layer is a gated recurrent unit layer with $64$ hidden units, using the \textit{inverse tangent} as activation function and the \textit{sigmoid} as recurrent activation function.
%
    A $10\%$ dropout rate was used to avoid the network overfitting.
%
    \item \textbf{Output}: the output layer is a fully connected layer of two units, using the \textit{softmax} as activation function.
%
\end{itemize}

The \textit{categorical cross entropy} loss function has been used to calculate the loss between the network output and the expected output.
%
Adam \cite{Kingma_2014} has been used as optimization algorithm to maximize the accuracy and minimize the loss of the network.

The weights of the input layer have not been further trained during the training of the model since they had already been trained on a vocabulary built on the training set.

\begin{table}[H]
\centering

\begin{tabular}{c|c|c}
    \textbf{Layer (type)}                  & \textbf{Output Shape}                 & \textbf{Param \#} \\
    \hline\hline
    \multicolumn{1}{l|}{input (Embedding)} & \multicolumn{1}{c|}{(None, 200, 300)} & \multicolumn{1}{r}{938,400} \\ \hdashline
    \multicolumn{1}{l|}{hidden\_1 (GRU)}   & \multicolumn{1}{c|}{(None, 200, 512)} & \multicolumn{1}{r}{427,776} \\ \hdashline
    \multicolumn{1}{l|}{hidden\_2 (GRU)}   & \multicolumn{1}{c|}{(None, 200, 64)}  & \multicolumn{1}{r}{37,056}  \\ \hdashline
    \multicolumn{1}{l|}{output (Dense)}    & \multicolumn{1}{c|}{(None, 2)}        & \multicolumn{1}{r}{130}     \\
    \hline\hline
    \multicolumn{3}{l}{Total params: $1,427,938$} \\
    \multicolumn{3}{l}{Trainable params: $489,538$} \\
    \multicolumn{3}{l}{Non-trainable params: $938,400$} \\
\end{tabular}

\caption{Summary of the model once it is compiled.}
\end{table}

\subsection{Training}

The model, once built, was trained for $25$ epochs on the training set using a batch size of $750$, a validation set was also provided to monitor the model's ability to generalize what it has learned from the training set.

\begin{figure}[H]
\centering

\includegraphics[width=\linewidth, height=\textheight, keepaspectratio]{res/training_viewer.jpeg}

\caption{Graph showing the progress of the loss and accuracy over the training set and validation set during training.
%
In blue is shown the trend on the training set and in green the trend on the validation set.}
\end{figure}

\subsection{Model assessment}

After the model was trained, its assessment was carried out.
%
To this end, the performance of the trained training set, validation set and test test was considered.
%
The metrics taken into consideration are loss and accuracy.

\begin{table}[H]
\centering

\begin{tabular}{c|c|c}
    \textbf{Data set}                   & \textbf{Loss}                 & \textbf{Accuracy} \\
    \hline\hline
    \multicolumn{1}{l|}{Training set}   & \multicolumn{1}{r|}{$0.0511$} & \multicolumn{1}{r}{$98.46\%$} \\ \hdashline
    \multicolumn{1}{l|}{Validation set} & \multicolumn{1}{r|}{$0.3402$} & \multicolumn{1}{r}{$88.96\%$} \\ \hdashline
    \multicolumn{1}{l|}{Test set}       & \multicolumn{1}{r|}{$0.3248$} & \multicolumn{1}{r}{$89.2\%$}  \\
\end{tabular}

\label{table:results}
\caption{Summary of the results obtained by the model after being trained.}
\end{table}

As indicated in table \ref{table:results} the accuracy achieved on the test set is $89.2\%$, improving the performance achieved using the Support Vector Machine algorithm with RBF kernel by $0.74$ percentage points\footnote{\url{https://medium.com/dataturks/using-machine-learning-to-fight-cyber-trolls-9bf0fa1c5df9}}.

\section{Evaluation}

The primary objective of this labor, from a business perspective, was to detect wheter a tweet was written by a Cyber-Troll or not given the textual content of the tweet.
%
During the assessment phase it was shown that the produced model was able to obtain excellent classification results at the expense of a poor generalization between the training set and the test set.

Unfortunately, to date it has not yet been made use of the produced model in real applications. However, given the results obtained during in vitro experiments it is plausible that the model would meet the business success criteria declared at the beginning of the project, if it was used in a real-world scenario.

\bibliography{references}
\bibliographystyle{ieeetr}

\end{document}
